FROM centos:7.6.1810

RUN yum update -y && yum clean all

RUN groupadd -r avisi -g 1000 && useradd -u 1000 -r -g avisi -m -d /opt/avisi -s /sbin/nologin -c "Avisi user" avisi && \
    chmod 755 /opt/avisi

WORKDIR /opt/avisi

USER avisi
