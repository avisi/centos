# CentOS base images

[![pipeline status](https://gitlab.com/avisi/centos/badges/master/pipeline.svg)](https://gitlab.com/avisi/centos/commits/master)

> Avisi CentOS 7 base images

This repository contains the Avisi CentOS Docker base images, that are used within various other projects. This image adds the `avisi` user and group, and creates the `/opt/avisi` directory.


## Table of Contents

- [Usage](#usage)
- [Contribute](#contribute)
- [License](#license)

## Usage

```dockerfile
FROM registry.gitlab.com/avisi/centos:7
```

### Available tags

| Tag | Notes |
|-----|-------|
| `7` | Latest CentOS 7 build |
| `latest` | Latest CentOS build, not necessarily CentOS 7 |

## Automated build

This image is build at least once a month automatically. All PR's are automatically build.

## Contribute

PRs accepted. All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/centos/issues).

## License

[MIT © Avisi B.V.](LICENSE)
